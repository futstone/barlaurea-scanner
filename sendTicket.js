import axios from 'axios'

const url = 'http://192.168.10.40:5000/api/tickets/use'
// Call our server with the extracted data from the QR-code (order_id)
export const useTicket = async data => {
  console.log('onUseTicket with data:', data)
  // Data is a string, we'll convert it to object for request payload
  const dataAsObj = JSON.parse(data)

  try {
    const response = await axios.post(url, dataAsObj)
    console.log(response)
    return true
  } catch (err) {
    console.log('Expected Error', err)
    return false
  }
}
