import React from 'react'
import { Text, View, StyleSheet, Button } from 'react-native'
import * as Permissions from 'expo-permissions'
import { BarCodeScanner } from 'expo-barcode-scanner'
import { useTicket } from './sendTicket'

export default class App extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false
  }

  async componentDidMount() {
    this.getPermissionsAsync()
  }

  // Camera requires permissions, this will fire a prompt to user
  // is called when the app starts
  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render() {
    const { hasCameraPermission, scanned } = this.state

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end'
        }}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />

        {scanned && (
          <Button
            title={'Tap to Scan Again (app updated 3)'}
            onPress={() => this.setState({ scanned: false })}
          />
        )}
      </View>
    )
  }
  // This will define what happens when the QR-code has
  // been successfully read
  handleBarCodeScanned = async ({ type, data }) => {
    // Just log the data because its pretty cool to play with
    console.log('Scanned QRCODE data: ', data)
    this.setState({ scanned: true })
    // Call our server with the extracted data from the QR-code (order_id)
    const success = await useTicket(data)
    // Inform the user whether the ticket was valid or not and successfully reclaimed or not
    if (success) {
      alert(`Ticket was valid and is now used.`)
    } else {
      alert(`INVALID TICKET`)
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
